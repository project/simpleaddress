<?php

/**
 * @file
 * Definition of Drupal\simpleaddress\Plugin\field\formatter\SimpleAddressDefaultFormatter.
 */

namespace Drupal\simpleaddress\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal;

/**
 * Plugin implementation of the 'simpleaddress_default' formatter.
 *
 * @FieldFormatter(
 *   id = "simpleaddress_default",
 *   module = "simpleaddress",
 *   label = @Translation("Address"),
 *   field_types = {
 *     "simpleaddress"
 *   }
 * )
 */
class SimpleAddressDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = array();
    $countries = Drupal::service('country_manager')->getList();
    $formats = Drupal::config('simpleaddress.settings')->get();
    foreach ($items as $delta => $item) {
      $address = '';
      $address .= '<div class="addressfield">';
      $address .= '<div itemscope itemtype="http://schema.org/PostalAddress">';
      $formatter = (isset($formats[drupal_strtolower($item->addressCountry)])) ?
        $formats[drupal_strtolower($item->addressCountry)] : $formats['default'];
      foreach($formatter as $line) {
        if(isset($item->{$line})) {
          if($line == 'addressCountry') {
            $item->{$line} = $countries[$item->{$line}];
          }
          $address .= '<span itemprop="' . $line . '">' . $item->{$line} . '</span>';
        }
      }
      $address .= '</div>';
      $address .= '</div>';

      $elements[$delta] = array('#markup' => $address);
    }
    return $elements;
  }
}
